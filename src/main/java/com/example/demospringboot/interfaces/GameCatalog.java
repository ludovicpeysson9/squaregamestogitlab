package com.example.demospringboot.interfaces;

import java.util.ArrayList;

public interface GameCatalog {

    public ArrayList<String> getGameId();

}
